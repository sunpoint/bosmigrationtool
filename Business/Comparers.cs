﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business
{
    public class Comparers
    {
        private ClientContext sourceCTX;
        private ClientContext destinationCTX;
        public Comparers(ClientContext sourceCtx, ClientContext destinationCtx)
        {
            this.sourceCTX = sourceCtx;
            this.destinationCTX = destinationCtx;
        }

        public void Compare (string listName, string sourceF, string destinationF)
        {
            List sourceList = sourceCTX.Web.Lists.GetByTitle(listName);
            List destinationList = destinationCTX.Web.Lists.GetByTitle(listName);

            CamlQuery sourceQuery = CamlQuery.CreateAllItemsQuery();
            ListItemCollection sourceItems = sourceList.GetItems(sourceQuery);
            ListItemCollection destinationItems = destinationList.GetItems(sourceQuery);

            sourceCTX.Load(sourceItems);
            sourceCTX.ExecuteQuery();

            destinationCTX.Load(destinationItems);
            destinationCTX.ExecuteQuery();

            var source = GetStringsList(sourceItems, sourceF);//.OrderByDescending(s => s).GroupBy(d => d);
            var destination = GetStringsList(destinationItems, destinationF);//.OrderByDescending(s => s).GroupBy(d => d);
            Console.WriteLine($"Source Count {source.Count()} Destination Count {destination.Count()}");
            Console.WriteLine("Field Value ============ SourceCount =============== DestinationCount");

            //foreach (var group in source)
            //{
            //    var sourceGroup = destination.FirstOrDefault(d => d.Key == group.Key);
            //    Console.WriteLine($"{group.Key} - {group.Count()} - {sourceGroup.Count()}");
            //}

            //foreach(var group in source)
            //{
            //    Console.WriteLine($"{group.Key} - {group.Count()}");
            //}
            //Console.ReadLine();
            //foreach (var group in destination)
            //{
            //    Console.WriteLine($"{group.Key} - {group.Count()}");
            //}
        }

        private List<string> GetStringsList(ListItemCollection items, string field)
        {
            var result = new List<string>();
            foreach (ListItem item in items)
            {
                var value = item[field] != null ? item[field].ToString() : string.Empty;
                result.Add(value);
            }
            Console.WriteLine($"{items.Context.Url} - {items.Count}");
            return result;
        }
    }
}
