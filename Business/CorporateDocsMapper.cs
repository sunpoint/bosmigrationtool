﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business
{
    class CorporateDocsMapper : DocumentLibraryBaseMapper
    {
        private readonly Dictionary<int, string> zCompanyMaster;
        public CorporateDocsMapper(ClientContext sourceCTX, ClientContext destinationCTX)
              : base(sourceCTX, destinationCTX)
        {
            zCompanyMaster = destinationCTX.GetLookupFromList("Z-Company Master", "Code");
            SourceListName = "Corporate Docs"; //"EMPLOYMENT%20CONTRACTS"; 
            DestinationListName = "Corporate Docs";
        }
        protected override void Map(ListItem source, ListItem destination)
        {
            MapLookups(source, destination);
        }

        private void MapLookups(ListItem source, ListItem destination)
        {
            destination.SetValueFromDictionary(source, zCompanyMaster, "Company", "Company_x0020_II_x0020_from_x0020_Z");
        }
    }
}
