﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BOSMigrationToolNetFramework.Helpers;

namespace BOSMigrationToolNetFramework.Business
{
    public class EmploymentMapper : DocumentLibraryBaseMapper
    {
        private readonly Dictionary<int, string> zCompanyMaster;
        private readonly Dictionary<int, string> zEmployerMaster;
        public EmploymentMapper(ClientContext sourceCTX, ClientContext destinationCTX)
            :base(sourceCTX, destinationCTX)
        {
            zCompanyMaster = destinationCTX.GetLookupFromList("Z-Company Master", "Code");
            zEmployerMaster = destinationCTX.GetLookupFromList("Z-Employee Master List", "Title");
            SourceListName = "Employment"; //"EMPLOYMENT%20CONTRACTS"; 
            DestinationListName = "Empolyment";
        }
        protected override void Map(ListItem source, ListItem destination)
        {
            MapLookups(source, destination);
        }

        private void MapLookups(ListItem source, ListItem destination)
        {
            destination.SetValueFromDictionary(source, zCompanyMaster, "Employer", "Employer2");
            destination.SetValueFromDictionary(source, zEmployerMaster, "Employee", "Empoyee_x0020_II");
        }
    }
}
