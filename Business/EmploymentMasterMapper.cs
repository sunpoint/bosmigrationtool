﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business
{
    public class EmploymentMasterMapper : BaseMapper
    {
        private Dictionary<int, string> zCompanyMasterList;

        public EmploymentMasterMapper(ClientContext source, ClientContext destination)
            :base(source, destination)
        {
            zCompanyMasterList = destination.GetLookupFromList("Z-State List Master", "Title");
        }
        
        protected override void Map(ListItem sourceItem, ListItem destinationItem)
        {
            //destinationItem["Title"] = sourceItem.ParseStringField("Title");
            //destinationItem["Status"] = sourceItem.ParseStringField("Status");
            //destinationItem.SetValueFromDictionary(sourceItem, zCompanyMasterList, "State"/*, "Company_x0020_Name"*/);
        }
    }
}
