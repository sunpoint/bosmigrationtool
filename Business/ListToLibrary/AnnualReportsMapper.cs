﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business.ListToLibrary
{
    class AnnualReportsMapper : ListToLibraryBaseMap
    {
        private Dictionary<int, string> zCompanyMaster;
        private Dictionary<int, string> zStateListMaster;
        public AnnualReportsMapper(ClientContext sourceCTX, ClientContext destinationCTX)
            : base(sourceCTX, destinationCTX)
        {
            SourceListName = "Annual Reports";
            DestinationListName = "Annual Reports";
            zCompanyMaster = DestinationCTX.GetLookupFromList("Z-Company Master", "Code");
            zStateListMaster = DestinationCTX.GetLookupFromList("Z-State List Master", "Title");
        }

        protected override void CustomMap(ListItem source, ListItem destination)
        {
            SetLookupFiels(source, destination);
        }

        private void SetLookupFiels(ListItem source, ListItem destination)
        {
            destination.SetValueFromDictionary(source, zCompanyMaster, "Company_x0020_Code", "Company_x0020_Name");
            destination.SetValueFromDictionary(source, zStateListMaster, "State");
        }
    }
}
