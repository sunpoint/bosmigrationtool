﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business.ListToLibrary
{
    class BusinessLicensesMapper : ListToLibraryBaseMap
    {
        private Dictionary<int, string> zCompanyMaster;
        private Dictionary<int, string> zRSOMaster;
        private Dictionary<int, string> zStateListMaster;
        public BusinessLicensesMapper(ClientContext sourceCTX, ClientContext destinationCTX)
            : base(sourceCTX, destinationCTX)
        {
            SourceListName = "Business Licenses";
            DestinationListName = "Business Licenses";
            zCompanyMaster = DestinationCTX.GetLookupFromList("Z-Company Master", "Title");
            zRSOMaster = DestinationCTX.GetLookupFromList("Z-RSO Master", "Office_x002f_Center_x0020_Name");
            zStateListMaster = DestinationCTX.GetLookupFromList("Z-State List Master", "Title");
        }

        protected override void CustomMap(ListItem source, ListItem destination)
        {
            SetLookupFiels(source, destination);
        }

        private void SetLookupFiels(ListItem source, ListItem destination)
        {
            destination.SetValueFromDictionary(source, zCompanyMaster, "Company");
            destination.SetValueFromDictionary(source, zRSOMaster, "Office_x002f_Center_x0020_Name");
            destination.SetValueFromDictionary(source, zStateListMaster, "State");
        }
    }
}
