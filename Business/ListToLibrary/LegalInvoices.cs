﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business.ListToLibrary
{
    class LegalInvoices : ListToLibraryBaseMap
    {
        private Dictionary<int, string> zCompanyMaster;
        private Dictionary<int, string> zStateListMaster;
        public LegalInvoices(ClientContext sourceCTX, ClientContext destinationCTX)
            : base(sourceCTX, destinationCTX)
        {
            SourceListName = "Legal Invoices";
            DestinationListName = "Legal Invoices";
            zCompanyMaster = DestinationCTX.GetLookupFromList("Z-Company Master", "Code");
            zStateListMaster = DestinationCTX.GetLookupFromList("Z-Law Firm Master", "Title");
        }

        protected override void CustomMap(ListItem source, ListItem destination)
        {
            SetLookupFiels(source, destination);
        }

        private void SetLookupFiels(ListItem source, ListItem destination)
        {
            destination.SetValueFromDictionary(source, zCompanyMaster, "Company");
            destination.SetValueFromDictionary(source, zStateListMaster, "Law_x0020_Firm");
        }
    }
}
