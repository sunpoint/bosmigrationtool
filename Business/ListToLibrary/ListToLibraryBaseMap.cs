﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business.ListToLibrary
{
    abstract class ListToLibraryBaseMap
    {
        protected readonly ClientContext SourceCTX;
        protected readonly ClientContext DestinationCTX;
        protected virtual string SourceListName { get; set; }
        protected virtual string DestinationListName { get; set; }

        public ListToLibraryBaseMap(ClientContext sourceCtx, ClientContext destinationCtx)
        {
            SourceCTX = sourceCtx;
            DestinationCTX = destinationCtx;
        }

        public void CreateFields()
        {
            List sourceList = SourceCTX.Web.Lists.GetByTitle(SourceListName);
            List destinationList = DestinationCTX.Web.Lists.GetByTitle(DestinationListName);

            SourceCTX.Load(sourceList);
            SourceCTX.ExecuteQuery();

            DestinationCTX.Load(destinationList);
            DestinationCTX.ExecuteQuery();

            FieldCollection fields = sourceList.Fields;
            SourceCTX.Load(fields);
            SourceCTX.ExecuteQuery();
            var fieldsToCopy = fields.Where(f => !f.ReadOnlyField
               && !f.Hidden
               && f.FieldTypeKind != FieldType.Lookup
               && f.InternalName != "ContentType"
               && f.InternalName != "Attachments"
               );

            foreach (var field in fieldsToCopy)
            {
                try
                {
                    var xmlValue = field.SchemaXml;
                    destinationList.Fields.AddFieldAsXml(xmlValue, true, AddFieldOptions.AddToDefaultContentType);
                    destinationList.Update();
                    DestinationCTX.ExecuteQuery();
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public List<string> Map()
        {
            List sourceList = SourceCTX.Web.Lists.GetByTitle(SourceListName);
            List destinationList = DestinationCTX.Web.Lists.GetByTitle(DestinationListName);
            List<string> exceptions = new List<string>();

            CamlQuery allItemsQuery = CamlQuery.CreateAllItemsQuery();

            ListItemCollection sourceItems = sourceList.GetItems(allItemsQuery);
            SourceCTX.Load(sourceItems);
            SourceCTX.ExecuteQuery();

            //ListItemCollection destinationItems = destinationList.GetItems(allItemsQuery);
            //DestinationCTX.Load(destinationItems);
            //DestinationCTX.ExecuteQuery();

            //Getting fields to copy
            FieldCollection fields = sourceList.Fields;
            SourceCTX.Load(fields);
            SourceCTX.ExecuteQuery();
            Web web = SourceCTX.Web;
            SourceCTX.Load(web);
            SourceCTX.ExecuteQuery();
            int i = 0;
            foreach (ListItem source in sourceItems)
            {
                //var exception = false;
                FileCollection attachments = null;
                try
                {
                    string src = string.Format("/sites/LPORT/Docs/lists/Corporate%20Log/Attachments/{0}",
                                   source.Id);
                    Folder attachmentsFolder = web.GetFolderByServerRelativeUrl(src);
                    SourceCTX.Load(attachmentsFolder);
                    attachments = attachmentsFolder.Files;
                    var hasAttachments = false;
                    try
                    {
                        SourceCTX.Load(attachments);
                        SourceCTX.ExecuteQuery();
                        hasAttachments = true;
                    }
                    catch (Exception ex)
                    {
                        hasAttachments = false;
                    }
                    if (hasAttachments)
                    {
                        foreach (File attachment in attachments)
                        {
                            FileInformation fileInfo = File.OpenBinaryDirect(SourceCTX, attachment.ServerRelativeUrl);
                            var nameWithoutExtension = $"/sites/AAHLegal/{DestinationListName}/" + System.IO.Path.GetFileNameWithoutExtension(attachment.ServerRelativeUrl);
                            var extension = System.IO.Path.GetExtension(attachment.ServerRelativeUrl);
                            var destinationFileUrl = nameWithoutExtension + extension;

                            if (DestinationCTX.HasPendingRequest)
                                DestinationCTX.ExecuteQuery();

                            if (SourceCTX.HasPendingRequest)
                                SourceCTX.ExecuteQuery();

                            File.SaveBinaryDirect(DestinationCTX, destinationFileUrl, fileInfo.Stream, true);

                            Map(fields, source, destinationFileUrl);
                            DestinationCTX.ExecuteQuery();
                        }
                    }
                    else
                    {
                        var bytes = System.IO.File.ReadAllBytes(@"C:\Users\dmitrip\Downloads\bosmigrationtool\bosmigrationtool\bin\Debug\Dummy Doc.docx");
                        using (var ms = new System.IO.MemoryStream(bytes))
                        {
                            var fileName = $"Dummy Doc {Guid.NewGuid().ToString()}.docx";
                            var destinationFileUrl = $"/sites/AAHLegal/{DestinationListName}/{fileName}";

                            if (DestinationCTX.HasPendingRequest)
                                DestinationCTX.ExecuteQuery();

                            if (SourceCTX.HasPendingRequest)
                                SourceCTX.ExecuteQuery();

                            File.SaveBinaryDirect(DestinationCTX, destinationFileUrl, ms, true);

                            Map(fields, source, destinationFileUrl);
                        }
                    }
                    DestinationCTX.ExecuteQuery();
                }
                catch (Exception ex)
                {
                    exceptions.Add($"Exception on listItem {source.Id}. Ex Message {ex.Message}");
                    continue;
                }

                i++;
                Console.Clear();
                Console.WriteLine($"{i} of {sourceItems.Count} items created");
                //break;
            }
            return exceptions;
        }

        private void Map(FieldCollection fields, ListItem source, string destinationFileUrl)
        {
            File uploadedFile = DestinationCTX.Web.GetFileByServerRelativeUrl(destinationFileUrl);
            DestinationCTX.Load(uploadedFile);
            DestinationCTX.ExecuteQuery();

            var checkedOut = false;
            if (uploadedFile.CheckOutType == CheckOutType.None)
            {
                uploadedFile.CheckOut();
                checkedOut = true;
            }

            var destinationListItem = uploadedFile.ListItemAllFields;
            Map(fields, source, destinationListItem);

            destinationListItem.Update();

            if (checkedOut)
            {
                destinationListItem.File.CheckIn("Check in by BosMigrationTool", CheckinType.OverwriteCheckIn);
            };

            //return destinationListItem;
        }

        private void Map(FieldCollection fields, ListItem source, ListItem destination)
        {
            var fieldsToUse = fields.Where(f => !f.ReadOnlyField
               && !f.Hidden
               && f.FieldTypeKind != FieldType.Lookup
               && f.InternalName != "ContentType"
               && f.InternalName != "Attachments"
               && f.InternalName != "AttachmentFiles"
               );

            foreach (Field field in fieldsToUse)
            {
                try
                {
                    //Console.WriteLine($"Trying to set field {field.InternalName} {field.StaticName} {field.Title}");

                    #region RINGI List SPECIFIC
                    //Specific for RINGI List
                    //if (field.InternalName == "Notes")
                    //    destination["Company_x0020_Name"] = source[field.InternalName];
                    //else if (field.InternalName == "Application_x0020_No_x002e_")
                    //    destination["Date"] = source[field.InternalName];
                    //else
                    #endregion

                    #region Business Licenses specific
                    //if (field.InternalName == "License_x0020_Type0")
                    //    destination["License_x0020_Type"] = source[field.InternalName];
                    //else if (field.InternalName == "Next_x0020_Due")
                    //    destination["Expiration_x0020_Date0"] = source[field.InternalName];
                    //else if (field.InternalName == "License_x0020_Type")
                    //    destination["License_x0020_Name"] = source[field.InternalName];
                    //else if (field.InternalName == "Notes")
                    //    destination["Notes0"] = source[field.InternalName];
                    //else if (field.InternalName == "Agency_x0020_TYpe")
                    //    destination["Agency_x0020_Type"] = source[field.InternalName];
                    //else
                    //    destination[field.InternalName] = source[field.InternalName];
                    #endregion

                    #region Physician
                    //if(field.InternalName == "Expiration_x0020_Date")
                    //    destination["Expiration_x0020_Date0"] = source[field.InternalName];
                    //else if(field.InternalName == "Status")
                    //    destination["License_x0020_Status"] = source[field.InternalName];
                    //else if (field.InternalName == "Date_x0020_of_x0020_Last_x0020_A")
                    //    destination["Date_x0020_of_x0020_Last_x0020_Action"] = source[field.InternalName];
                    //else if (field.InternalName == "Note")
                    //    destination["Notes0"] = source[field.InternalName];
                    //else
                    //    destination[field.InternalName] = source[field.InternalName];

                    #endregion

                    #region AnnualReports specific
                    if (field.InternalName == "Entity_x0020_No_x002e_")
                        destination["Entity_x0020__x0023_"] = source[field.InternalName];
                    else if (field.InternalName == "Incorporation_x0020_Date")
                        destination["Incorporation_x0020_or_x0020_Qualification_x0020_Date"] = source[field.InternalName];
                    else if (field.InternalName == "Registered_x0020_Agent_x0020_Add")
                        destination["Registered_x0020_Agent_x0020_Address"] = source[field.InternalName];
                    else if (field.InternalName == "Principal_x0020_Place_x0020_of_x")
                        destination["Principal_x0020_Place_x0020_of_x0020_Business"] = source[field.InternalName];
                    else if (field.InternalName == "Directors")
                        destination["Current_x0020_Directors"] = source[field.InternalName];
                    else if (field.InternalName == "Date_x0020_Director_x0020_Electe")
                        destination["Date_x0020_Directors_x0020_Elected"] = source[field.InternalName];
                    else if (field.InternalName == "Date_x0020_Officers_x0020_Appoin")
                        destination["Date_x0020_Officers_x0020_Appointed"] = source[field.InternalName];
                    else if (field.InternalName == "Last_x0020_Annual_x0020_Report")
                        destination["Last_x0020_Report_x0020_Filed"] = source[field.InternalName];
                    else if (field.InternalName == "_x0052_eq2")
                        destination["Requirement"] = source[field.InternalName];
                    else if (field.InternalName == "Notes")
                        destination["Annual_x0020_Report_x0020_Notes"] = source[field.InternalName];
                    else if (field.InternalName == "No_x002e__x0020_of_x0020_Directo")
                        destination["Req_x0027_d_x0020__x0023__x0020_of_x0020_Directors"] = source[field.InternalName];
                    else if (field.InternalName == "Company_x0020_Code_x0020__x002d_")
                        destination["Company_x0020_Code_x0020__x002d__x0020_Short"] = source[field.InternalName];
                    else if (field.InternalName == "Officers")
                        destination["Current_x0020_Officers"] = source[field.InternalName];
                    else
                        destination[field.InternalName] = source[field.InternalName];
                    #endregion
                    #region Legal Invoices Specific
                    //if (field.InternalName == "TM_x0023_")
                    //    destination["Matter_x0020_No_x002e_"] = source[field.InternalName];
                    //else if (field.InternalName == "Date_x0020_Sent_x0020_to_x0020_A")
                    //    destination["Date_x0020_Sent_x0020_to_x0020_Accounting"] = source[field.InternalName];
                    //else if (field.InternalName == "Notes")
                    //    destination["Notes0"] = source[field.InternalName];
                    //else
                    //    destination[field.InternalName] = source[field.InternalName];
                    #endregion
                    //destination[field.InternalName] = source[field.InternalName];
                }
                catch (Exception ex)
                {
                    continue;
                }
            }
            CustomMap(source, destination);
        }

        protected abstract void CustomMap(ListItem source, ListItem destination);
    }
}
