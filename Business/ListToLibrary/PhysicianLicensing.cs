﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business.ListToLibrary
{
    class PhysicianLicensing : ListToLibraryBaseMap
    {
        private Dictionary<int, string> zPhysicianMaster;
        private Dictionary<int, string> zStateMaster;

        public PhysicianLicensing(ClientContext sourceCTX, ClientContext destinationCTX)
            : base(sourceCTX, destinationCTX)
        {
            SourceListName = "Physician Licensing";
            DestinationListName = "Physician Licensing";
            zPhysicianMaster = DestinationCTX.GetLookupFromList("Z-Physician List Master", "Title");
            zStateMaster = destinationCTX.GetLookupFromList("Z-State List Master", "State_x0020_Abbreviation");
        }

        protected override void CustomMap(ListItem source, ListItem destination)
        {
            SetLookupFiels(source, destination);
        }

        private void SetLookupFiels(ListItem source, ListItem destination)
        {
            destination.SetValueFromDictionary(source, zPhysicianMaster, "Physician_x0020_Name");
            destination.SetValueFromDictionary(source, zPhysicianMaster, "Supervising_x0020_Physician");
            destination.SetValueFromDictionary(source, zPhysicianMaster, "Alternate_x0020_Physician");
            destination.SetValueFromDictionary(source, zStateMaster, "State");
        }
    }
}
