﻿using BOSMigrationToolNetFramework.Business.ListToLibrary;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business
{
    class RINGIListMapper : ListToLibraryBaseMap
    {

        public RINGIListMapper(ClientContext sourceCTX, ClientContext destinationCTX)
            :base(sourceCTX, destinationCTX)
        {
            SourceListName = "RINGI List";
            DestinationListName = "RINGI List";
        }
        protected override void CustomMap(ListItem source, ListItem destination)
        {
            Console.WriteLine("Nothing here");
        }
    }
}
