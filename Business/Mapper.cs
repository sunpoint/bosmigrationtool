﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business
{
    public abstract class BaseMapper
    {
        protected readonly ClientContext SourceCtx;
        protected readonly ClientContext DestinationCtx;

        protected BaseMapper(ClientContext source, ClientContext destiation)
        {
            SourceCtx = source;
            DestinationCtx = destiation;
        }

        public virtual void Map(string sourceListName, string destinationListName)
        {
            List sourceList = SourceCtx.Web.Lists.GetByTitle(sourceListName);
            List destinationList = DestinationCtx.Web.Lists.GetByTitle(destinationListName);

            CamlQuery sourceQuery = CamlQuery.CreateAllItemsQuery();
            ListItemCollection sourceItems = sourceList.GetItems(sourceQuery);
            ListItemCollection destinationItems = destinationList.GetItems(sourceQuery);

            FieldCollection fieldCollection = sourceList.Fields;

            SourceCtx.Load(fieldCollection);

            SourceCtx.Load(sourceItems);
            SourceCtx.ExecuteQuery();

            DestinationCtx.Load(destinationItems);
            DestinationCtx.ExecuteQuery();

            int i = 0;
            //for (int i = 0; i < destinationItems.Count; i++)
            //{
            //    Map(sourceItems[i], destinationItems[i]);
            //    destinationItems[i].Update();
            //    DestinationCtx.ExecuteQuery();
            //    Console.WriteLine($"{i} of {sourceItems.Count} created");
            //}
            foreach (ListItem item in sourceItems)
            {
                i++;
                if (i >= 266)
                {
                    ListItemCreationInformation creationInfo = new ListItemCreationInformation();
                    ListItem destinationItem = destinationList.AddItem(creationInfo);
                    destinationItem.Update();
                    DestinationCtx.ExecuteQuery();
                    Map(fieldCollection, item, destinationItem);
                    //Map(item, destinationItem);
                    destinationItem.Update();
                    DestinationCtx.ExecuteQuery();
                }
                
               
                //Console.Clear();
                Console.WriteLine($"{i} of {sourceItems.Count} created");
                //break;
            }
        }

        private void Map(FieldCollection fields, ListItem source, ListItem destination)
        {
            var fieldsToUse = fields.Where(f => !f.ReadOnlyField 
                && !f.Hidden 
                && f.FieldTypeKind != FieldType.Lookup 
                && f.InternalName != "ContentType"
                && f.InternalName != "Attachments"
                );

            foreach (Field field in fieldsToUse)
            {
                try
                {

                    Console.WriteLine($"Trying to set field {field.InternalName} {field.StaticName} {field.Title}");
                    destination[field.InternalName] = source[field.InternalName];
                }
                catch(Exception ex)
                {
                    Console.WriteLine($"Failed11 for field {field.InternalName} {field.StaticName} {field.Title}");
                    continue;
                }
            }
        }

        protected abstract void Map(ListItem source, ListItem destination);
    }
}
