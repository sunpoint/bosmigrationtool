﻿using BOSMigrationToolNetFramework.Business.Mappers;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business
{
    public abstract class DocumentLibraryBaseMapper
    {
        protected readonly ClientContext SourceCTX;
        protected readonly ClientContext DestinationCTX;
        protected virtual string SourceListName { get; set; }
        protected virtual string DestinationListName { get; set; }
        public DocumentLibraryBaseMapper(ClientContext sourceCtx, ClientContext destinationCtx)
        {
            SourceCTX = sourceCtx;
            DestinationCTX = destinationCtx;
        }

        public void Map()
        {
            List sourceList = SourceCTX.Web.Lists.GetByTitle(SourceListName);
            List destinationList = DestinationCTX.Web.Lists.GetByTitle(DestinationListName);

            CamlQuery allItemsQuery = CamlQuery.CreateAllItemsQuery();

            ListItemCollection sourceItems = sourceList.GetItems(allItemsQuery);
            SourceCTX.Load(sourceItems);
            SourceCTX.ExecuteQuery();

            ListItemCollection destinationItems = destinationList.GetItems(allItemsQuery);
            DestinationCTX.Load(destinationItems);
            DestinationCTX.ExecuteQuery();

            var srcLibraryName = sourceItems[0].FieldValues["FileDirRef"].ToString();
            string[] srcUrlSplit = srcLibraryName.Split('/');
            srcLibraryName = srcUrlSplit[srcUrlSplit.Length - 1];
            int i = 0;
            Console.WriteLine(sourceItems.Count);
            Console.WriteLine(destinationItems.Count);
            foreach (ListItem source in sourceItems)
            {
                var fileName = source["FileRef"].ToString();
                string[] fileNames = fileName.Split(new string[] { srcLibraryName }, StringSplitOptions.None);
                fileName = fileNames[fileNames.Count() - 1];

                File file = source.File;

                try
                {
                    SourceCTX.Load(file);
                    SourceCTX.ExecuteQuery();
                }
                catch (Exception ex)
                {
                    continue;
                }

                FileInformation fileInfo = File.OpenBinaryDirect(SourceCTX, file.ServerRelativeUrl);
                var nameWithoutExtension = $"/sites/AAHLegal/{DestinationListName}/" + System.IO.Path.GetFileNameWithoutExtension(file.ServerRelativeUrl);
                var extension = System.IO.Path.GetExtension(file.ServerRelativeUrl);
                var destinationFileUrl = nameWithoutExtension + extension;

                if (DestinationCTX.HasPendingRequest)
                    try { DestinationCTX.ExecuteQuery(); }
                    catch (Exception ex)
                    {
                        continue;
                    }


                if (SourceCTX.HasPendingRequest)
                    SourceCTX.ExecuteQuery();

                destinationFileUrl = destinationFileUrl.Replace(" ", "%20");

                if (!TryGetFileByServerRelativeUrl(DestinationCTX.Web, destinationFileUrl))
                    File.SaveBinaryDirect(DestinationCTX, destinationFileUrl, fileInfo.Stream, false);

                var uploadedFile = DestinationCTX.Web.GetFileByServerRelativeUrl(destinationFileUrl);
                DestinationCTX.Load(uploadedFile);
                DestinationCTX.ExecuteQuery();

                var checkedOut = false;
                if (uploadedFile.CheckOutType == CheckOutType.None)
                {
                    uploadedFile.CheckOut();
                    checkedOut = true;
                }

                //Getting fields to copy
                FieldCollection fields = sourceList.Fields;
                SourceCTX.Load(fields);
                SourceCTX.ExecuteQuery();

                var destinationListItem = uploadedFile.ListItemAllFields;
                MapFields(fields, source, destinationListItem);
                Map(source, destinationListItem);

                destinationListItem.Update();
                DestinationCTX.ExecuteQuery();

                if (checkedOut)
                {
                    destinationListItem.File.CheckIn("Check in by BosMigrationTool", CheckinType.OverwriteCheckIn);
                }
                i++;
                Console.Clear();
                Console.WriteLine($"{i} of {sourceItems.Count} items created");
                //break;
            }
        }

        private void MapFields(FieldCollection fields, ListItem source, ListItem destination)
        {
            var fieldsToUse = fields.Where(f => !f.ReadOnlyField
               && !f.Hidden
               && f.FieldTypeKind != FieldType.Lookup
               && f.InternalName != "ContentType"
               && f.InternalName != "Attachments"
               );

            foreach (Field field in fieldsToUse)
            {
                try
                {
                    //Console.WriteLine($"Trying to set field {field.InternalName} {field.StaticName} {field.Title}");
                    destination[field.InternalName] = source[field.InternalName];
                }
                catch (Exception ex)
                {
                    //Console.WriteLine($"Failed11 for field {field.InternalName} {field.StaticName} {field.Title}");
                    continue;
                }
            }
        }
        /*public void Map()
        {
            List sourceList = SourceCTX.Web.Lists.GetByTitle(SourceListName);
            List destinationList = DestinationCTX.Web.Lists.GetByTitle(DestinationListName);

            //CamlQuery allItemsQuery = CamlQuery.CreateAllItemsQuery();
            CamlQuery camlQuery = new CamlQuery();
            camlQuery.ViewXml = "<View Scope='RecursiveAll'><RowLimit>5000</RowLimit></View>";

            ListItemCollection sourceItems = sourceList.GetItems(camlQuery);
            SourceCTX.Load(sourceItems);
            SourceCTX.ExecuteQuery();

            ListItemCollection destinationItems = destinationList.GetItems(camlQuery);
            DestinationCTX.Load(destinationItems);
            DestinationCTX.ExecuteQuery();
            Folder ff = sourceList.RootFolder;
            FolderCollection fcol = sourceList.RootFolder.Folders; // here you will save the folder info inside a Folder Collection list
            List<string> lstFile = new List<string>();
            FileCollection ficol = sourceList.RootFolder.Files;   // here you will save the File names inside a file Collection list 
            // ------informational -------
            SourceCTX.Load(ff);
            SourceCTX.Load(sourceList);
            SourceCTX.Load(sourceList.RootFolder);
            SourceCTX.Load(sourceList.RootFolder.Folders);
            SourceCTX.Load(sourceList.RootFolder.Files);
            SourceCTX.ExecuteQuery();
            foreach (Folder f in fcol)
            {
                Console.WriteLine("Folder Name : " + f.Name);
                SourceCTX.Load(f.Files);
                SourceCTX.ExecuteQuery();
                FileCollection fileCol = f.Files;
                foreach (File file in fileCol)
                {
                    lstFile.Add(file.Name);
                    Console.WriteLine(" File Name : " + file.Name);

                }
            }

            var srcLibraryName = sourceItems[0].FieldValues["FileDirRef"].ToString();
            string[] srcUrlSplit = srcLibraryName.Split('/');
            srcLibraryName = srcUrlSplit[srcUrlSplit.Length - 1];
            int i = 0;
            Console.WriteLine(sourceItems.Count);
            Console.WriteLine(destinationItems.Count);
            bool isAddedForms = false;
            foreach (ListItem source in sourceItems)
            {
                var fileref = source["FileLeafRef"].ToString();

                foreach (Folder f in fcol)
                {
                    //if (f.Name == fileref)
                    //{
                    //    lstFile.Add(fileref);
                    //    FileCollection fileCol = f.Files;
                    //    foreach (File fileItem in fileCol)
                    //    {
                    //        lstFile.Add(fileItem.Name);
                    //        SourceCTX.Load(fileItem);
                    //        SourceCTX.ExecuteQuery();
                    //        FileInformation fileInfo = File.OpenBinaryDirect(SourceCTX, fileItem.ServerRelativeUrl);
                    //        //for my test library
                    //        var nameWithoutExtension = $"/sites/Sunpoint/{DestinationListName}/" + System.IO.Path.GetFileNameWithoutExtension(fileItem.ServerRelativeUrl);
                    //        //var nameWithoutExtension = $"/sites/AAHLegal/{DestinationListName}/" + System.IO.Path.GetFileNameWithoutExtension(file.ServerRelativeUrl);
                    //        var extension = System.IO.Path.GetExtension(fileItem.ServerRelativeUrl);
                    //        var destinationFileUrl = nameWithoutExtension + extension;

                    //        if (DestinationCTX.HasPendingRequest)
                    //            DestinationCTX.ExecuteQuery();

                    //        if (SourceCTX.HasPendingRequest)
                    //            SourceCTX.ExecuteQuery();

                    //        File.SaveBinaryDirect(DestinationCTX, destinationFileUrl, fileInfo.Stream, true);
                    //        var uploadedFile = DestinationCTX.Web.GetFileByServerRelativeUrl(destinationFileUrl);
                    //        DestinationCTX.Load(uploadedFile);
                    //        DestinationCTX.ExecuteQuery();

                    //        var checkedOut = false;
                    //        if (uploadedFile.CheckOutType == CheckOutType.None)
                    //        {
                    //            uploadedFile.CheckOut();
                    //            checkedOut = true;
                    //        }

                    //        //Getting fields to copy
                    //        FieldCollection fields = sourceList.Fields;
                    //        SourceCTX.Load(fields);
                    //        SourceCTX.ExecuteQuery();
                    //        source["FileLeafRef"] = uploadedFile.Name;
                    //        var destinationListItem = uploadedFile.ListItemAllFields;
                    //        MapFields(fields, source, destinationListItem);
                    //        Map(source, destinationListItem);

                    //        destinationListItem.Update();
                    //        //DestinationCTX.ExecuteQuery();

                    //        if (checkedOut)
                    //        {
                    //            destinationListItem.File.CheckIn("Check in by BosMigrationTool", CheckinType.OverwriteCheckIn);
                    //        }
                    //        i++;
                    //        Console.WriteLine($"{i} of {sourceItems.Count} items created");

                    //    }
                    //}
                    if (f.Name == "Forms" && !isAddedForms)
                    {
                        FileCollection fileCol = f.Files;
                        foreach (File fileItem in fileCol)
                        {
                            lstFile.Add(fileItem.Name);
                            SourceCTX.Load(fileItem);
                            SourceCTX.ExecuteQuery();
                            FileInformation fileInfo = File.OpenBinaryDirect(SourceCTX, fileItem.ServerRelativeUrl);
                            //for my test library
                            //var nameWithoutExtension = $"/sites/Sunpoint/{DestinationListName}/" + System.IO.Path.GetFileNameWithoutExtension(fileItem.ServerRelativeUrl);
                            var nameWithoutExtension = $"/sites/AAHLegal/AHG%20Model%20Releases/" + System.IO.Path.GetFileNameWithoutExtension(fileItem.ServerRelativeUrl);
                            var extension = System.IO.Path.GetExtension(fileItem.ServerRelativeUrl);
                            var destinationFileUrl = nameWithoutExtension + extension;
                            //destinationFileUrl = destinationFileUrl.Replace(" ", "%20");
                            if (DestinationCTX.HasPendingRequest)
                                DestinationCTX.ExecuteQuery();

                            if (SourceCTX.HasPendingRequest)
                                SourceCTX.ExecuteQuery();

                            File.SaveBinaryDirect(DestinationCTX, destinationFileUrl, fileInfo.Stream, true);
                            var uploadedFile = DestinationCTX.Web.GetFileByServerRelativeUrl(destinationFileUrl);
                            DestinationCTX.Load(uploadedFile);
                            DestinationCTX.ExecuteQuery();

                            var checkedOut = false;
                            if (uploadedFile.CheckOutType == CheckOutType.None)
                            {
                                uploadedFile.CheckOut();
                                checkedOut = true;
                            }
                            i++;
                            Console.WriteLine($"{i} of {sourceItems.Count} items created");
                        }
                        isAddedForms = true;
                    }

                }
                //if (!lstFile.Contains(fileref))
                //{
                //    var fileName = source["FileRef"].ToString();
                //    string[] fileNames = fileName.Split(new string[] { srcLibraryName }, StringSplitOptions.None);
                //    fileName = fileNames[fileNames.Count() - 1];

                //    File file = source.File;
                //    SourceCTX.Load(file);
                //    SourceCTX.ExecuteQuery();

                //    FileInformation fileInfo = File.OpenBinaryDirect(SourceCTX, file.ServerRelativeUrl);
                //    var nameWithoutExtension = $"/sites/Sunpoint/{DestinationListName}/" + System.IO.Path.GetFileNameWithoutExtension(file.ServerRelativeUrl);
                //    var extension = System.IO.Path.GetExtension(file.ServerRelativeUrl);
                //    var destinationFileUrl = nameWithoutExtension + extension;

                //    if (DestinationCTX.HasPendingRequest)
                //        DestinationCTX.ExecuteQuery();

                //    if (SourceCTX.HasPendingRequest)
                //        SourceCTX.ExecuteQuery();

                //    File.SaveBinaryDirect(DestinationCTX, destinationFileUrl, fileInfo.Stream, true);
                //    var uploadedFile = DestinationCTX.Web.GetFileByServerRelativeUrl(destinationFileUrl);
                //    DestinationCTX.Load(uploadedFile);
                //    DestinationCTX.ExecuteQuery();

                //    var checkedOut = false;
                //    if (uploadedFile.CheckOutType == CheckOutType.None)
                //    {
                //        uploadedFile.CheckOut();
                //        checkedOut = true;
                //    }

                //    //Getting fields to copy
                //    FieldCollection fields = sourceList.Fields;
                //    SourceCTX.Load(fields);
                //    SourceCTX.ExecuteQuery();

                //    var destinationListItem = uploadedFile.ListItemAllFields;
                //    MapFields(fields, source, destinationListItem);
                //    Map(source, destinationListItem);

                //    destinationListItem.Update();
                //    DestinationCTX.ExecuteQuery();

                //    if (checkedOut)
                //    {
                //        destinationListItem.File.CheckIn("Check in by BosMigrationTool", CheckinType.OverwriteCheckIn);
                //    }
                //    i++;
                //    Console.WriteLine($"{i} of {sourceItems.Count} items created");
                //}
            }
        }
        */
        protected abstract void Map(ListItem source, ListItem destination);

        private bool TryGetFileByServerRelativeUrl(Web web, string fileUrl) 
        {
            var ctx = web.Context;
            try {
                File file = web.GetFileByServerRelativeUrl(fileUrl);
                ctx.Load(file);
                ctx.ExecuteQuery();
                return true;
            }catch(ServerException ex) 
            {
                if(ex.ServerErrorTypeName == "System.IO.FileNotFountException") 
                {
                    return false;
                }
                return false;
            }
            catch(Exception exp) 
            {
                return false;
            }
        }
    }
}
