﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System.Collections.Generic;

namespace BOSMigrationToolNetFramework.Business.Mappers
{
    class GeneralDocumentsMapper : DocumentLibraryBaseMapper
    {
        private Dictionary<int, string> zClientMaster;
        private Dictionary<int, string> zCompanyMaster;
        private Dictionary<int, string> zDocListMaster;
        private Dictionary<int, string> zDocTypeMaster;

        #region Fields
        //string fields
        private readonly string Title = "Title";
        private readonly string Description = "Description";
        private readonly string Comments = "Comments";

        //dateTimeFields
        private readonly string Date = "Date";
        private readonly string ExpirationDate = "Expiration Date";
        private readonly string NoticeOfTerminationDue = "Notice of Termination Due";
        private readonly string NoticeOfRenewalDue = "Notice of Renewal Due";
        private readonly string MinPurchReqCheckIn = "Min Purch Req Check-in";
        private readonly string CertifOfInsuranceRenewal = "Certif of Insurance Renewal";
        private readonly string MiscTrigger = "Misc Trigger";

        //lookup
        private readonly string Company = "Company";
        private readonly string Document = "Document";
        private readonly string DocType = "Doc_x0020_Type";
        private readonly string Matter = "Matter0";

        //bool fields
        private readonly string Active = "Active?";
        private readonly string NoticeOfRenewalDone = "Notice of Renewal - Done?";
        private readonly string NoticeOfTerminationDone = "Notice of Termination - Done?";
        private readonly string ExpirationDone = "Expiration - Done?";
        #endregion

        public GeneralDocumentsMapper(ClientContext sourceCtx, ClientContext destinationCTX)
            :base(sourceCtx, destinationCTX)
        {
            SourceListName = "General Documents";
            DestinationListName = "General Documents";
            zCompanyMaster = DestinationCTX.GetLookupFromList("Z-Company Master", "Code");
            zDocListMaster = DestinationCTX.GetLookupFromList("Z-Doc List Master", "Title");
            zDocTypeMaster = DestinationCTX.GetLookupFromList("Z-Doc Type Master", "Title");
            zClientMaster = DestinationCTX.GetLookupFromList("Z-Client Master", "Title");
        }

        protected override void Map(ListItem source, ListItem destination)
        {
            //SetStringFields(source, destination);
            //SetDateTimeFiels(source, destination);
            //SetBoolFields(source, destination);
            SetLookupFiels(source, destination);
        }

        private void SetStringFields(ListItem source, ListItem destination)
        {
            destination[Company] = source.ParseStringField(Company);
            destination[Description] = source.ParseStringField(Description);
            destination[Comments] = source.ParseStringField(Comments);
        }

        private void SetDateTimeFiels(ListItem source, ListItem destination)
        {
            destination[Date] = source.ParseDateTimeField(Date);
            destination[ExpirationDate] = source.ParseDateTimeField(ExpirationDate);
            destination[NoticeOfTerminationDue] = source.ParseDateTimeField(NoticeOfTerminationDue);
            destination[NoticeOfRenewalDue] = source.ParseDateTimeField(NoticeOfRenewalDue);
            destination[MinPurchReqCheckIn] = source.ParseDateTimeField(MinPurchReqCheckIn);
            destination[CertifOfInsuranceRenewal] = source.ParseDateTimeField(CertifOfInsuranceRenewal);
            destination[MiscTrigger] = source.ParseDateTimeField(MiscTrigger);
        }

        private void SetBoolFields(ListItem source, ListItem destination)
        {
            destination[Active] = source.ParseBoolField(Active);
            destination[ExpirationDone] = source.ParseBoolField(ExpirationDone);
            destination[NoticeOfRenewalDone] = source.ParseBoolField(NoticeOfRenewalDone);
            destination[NoticeOfTerminationDone] = source.ParseBoolField(NoticeOfTerminationDone);
        }

        private void SetLookupFiels(ListItem source, ListItem destination)
        {
            destination.SetValueFromDictionary(source, zDocTypeMaster, DocType, "Doc_x0020_Type");
            destination.SetValueFromDictionary(source, zDocListMaster, Document, "New_x0020_Doc_x0020_Type");
            destination.SetValueFromDictionary(source, zClientMaster, Matter, "New_x0020_Matter");
            destination.SetValueFromDictionary(source, zCompanyMaster, Company, "CompanyNew");
        }
    }
}
