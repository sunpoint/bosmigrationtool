﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business.Mappers
{
    public interface IListItemMapper
    {
        string SourceList { get; set; }
        string DestinationList { get; set; }
        void Map(ListItem source, ListItem destination);
    }
}
