﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business.Mappers
{
    public class PhysicanAgreementsMaper : DocumentLibraryBaseMapper
    {
        private Dictionary<int, string> zMasterList;

        // loocup
        public string NameOfProfessional = "Title";
        public PhysicanAgreementsMaper(ClientContext sourceCtx, ClientContext destinationCtx)
    : base(sourceCtx, destinationCtx)
        {
            zMasterList = DestinationCTX.GetLookupFromList("Z-Physician List Master", NameOfProfessional);
            SourceListName = "Physician Agreements";
            DestinationListName = SourceListName;
        }

        protected override void Map(ListItem source, ListItem destination)
        {
            SetLookupFiels(source, destination);
        }

        private void SetLookupFiels(ListItem source, ListItem destination)
        {
            destination.SetValueFromDictionary(source, zMasterList, "Physician", "Physician_x0020_II");
        }
    }
}
