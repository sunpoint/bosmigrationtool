﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business.Mappers
{
    public class RealPropertyMapper : DocumentLibraryBaseMapper
    {
        private Dictionary<int, string> zMasterList;   
        //string fields
        private readonly string Company = "Company";
        private readonly string Address = "Address";
        private readonly string Title = "Title";
        private readonly string Matter = "Matter";
        private readonly string Lessor = "Lessor";
        private readonly string Status = "Status";
        private readonly string Seller = "Seller";
        private readonly string Buyer = "Buyer";
        private readonly string Comments = "Comments";

        //dateTimeFields
        private readonly string DateOfAgmtDoc = "Date of Agmt/Doc";
        private readonly string CommencementDate = "Commencement Date";
        private readonly string EndDate = "End Date";

        //lookup
        private readonly string StreetAddres = "Street_x0020_Address";

        //bool fields
        private readonly string Active = "Active";

        public RealPropertyMapper(ClientContext sourceCtx, ClientContext destinationCtx)
            :base(sourceCtx, destinationCtx)
        {
            zMasterList = DestinationCTX.GetLookupFromList("Z-RSO Master", StreetAddres);
            SourceListName = "Real Property";
            DestinationListName = SourceListName;
        }

        protected override void Map(ListItem source, ListItem destination)
        {
            //SetStringFields(source, destination);
            //SetDateTimeFiels(source, destination);
            //destination[Active] = source.ParseBoolField(Active);
            SetLookupFiels(source, destination);
        }

        private void SetStringFields(ListItem source, ListItem destination)
        {
            destination[Company] = source.ParseStringField(Company);
            destination[Address] = source.ParseStringField(Address);
            destination[Title] = source.ParseStringField(Title);
            destination[Matter] = source.ParseStringField(Matter);
            destination[Lessor] = source.ParseStringField(Lessor);
            destination[Status] = source.ParseStringField(Status);
            destination[Seller] = source.ParseStringField(Seller);
            destination[Buyer] = source.ParseStringField(Buyer);
            destination[Comments] = source.ParseStringField(Comments);
        }

        private void SetDateTimeFiels(ListItem source, ListItem destination)
        {
            destination[DateOfAgmtDoc] = source.ParseDateTimeField(DateOfAgmtDoc);
            destination[CommencementDate] = source.ParseDateTimeField(CommencementDate);
            destination[EndDate] = source.ParseDateTimeField(EndDate);
        }

        private void SetLookupFiels(ListItem source, ListItem destination)
        {
            destination.SetValueFromDictionary(source, zMasterList, StreetAddres);
        }
    }
}
