﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business
{
    class PatentsMapper : BaseMapper
    {
        private readonly Dictionary<int, string> zCompanyMasterList;
        private readonly Dictionary<int, string> zCountries;
        public PatentsMapper(ClientContext source, ClientContext destination)
            :base(source, destination)
        {
            zCompanyMasterList = destination.GetLookupFromList("Z-Company Master", "Title");
            Console.WriteLine("Z-Company Master retrieved");
            zCountries = destination.GetLookupFromList("Z-Countries", "Title");
            Console.WriteLine("Z-Countries retrieved");
        }

        protected override void Map(ListItem sourceItem, ListItem destinationItem)
        {
            //SetStringFields(sourceItem, destinationItem);
            //SetDateTimeFields(sourceItem, destinationItem);
            //SetLoookupFields(sourceItem, destinationItem);
        }

        private void SetDateTimeFields(ListItem destination, ListItem source)
        {
            destination["Filing_x0020x_Date"] = source.ParseDateTimeField("Filing_x0020x_Date");
            destination["Publication_x0020_Date"] = source.ParseDateTimeField("Publication_x0020_Date");
            destination["Registration_x0020_Date"] = source.ParseDateTimeField("Registration_x0020_Date");
            destination["Due_x0020_Date"] = source.ParseDateTimeField("Due_x0020_Date");
        }

        private void SetStringFields(ListItem source, ListItem destination)
        {
            destination["Title"] = source.ParseStringField("Title");
            destination["Application_x0020_No_x002e_"] = source.ParseStringField("Application_x0020_No_x002e_");
            destination["Publication_x0020_No_x002e_"] = source.ParseStringField("Publication_x0020_No_x002e_");
            destination["Patent_x0020_No_x002e_"] = source.ParseStringField("Patent_x0020_No_x002e_");
            destination["Next_x0020_Action_x0020_Due"] = source.ParseStringField("Next_x0020_Action_x0020_Due");
            destination["Status"] = source.ParseStringField("Status");
        }

        private void SetLoookupFields(ListItem source, ListItem destination)
        {
            Console.WriteLine("Tryying to set Inventor_x002f_Assignee");
            //destination.SetValueFromDictionary(source, zCompanyMasterList, "Inventor_x002f_Assignee");
            Console.WriteLine("Tryying to set Country");
            //destination.SetValueFromDictionary(source, zCountries, "Country");
        }
    }
}
