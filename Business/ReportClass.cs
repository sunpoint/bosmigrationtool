﻿using Microsoft.SharePoint.Client;
using SP=Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BOSMigrationToolNetFramework.Business
{
    public static class ReportClass
    {
        public static string GetSites(ClientContext source)
        {
            string result = "";
            WebCollection colWeb = source.Web.Webs;
            source.Load(colWeb);
            source.ExecuteQuery();
            var csv = new StringBuilder();
            Web webRoot = source.Site.RootWeb;
            source.Load(webRoot);
            source.ExecuteQuery();
            try
            {
                //var docTemplateCol = web.ListTemplates;
                //source.Load(docTemplateCol);
                //source.ExecuteQuery();
                ListCollection colListRoot = webRoot.Lists;
                //source.Load(colListRoot, items => items.Include(item => item.LastItemModifiedDate, item => item.BaseType, item => item.Created, item => item.Title, item => item.SchemaXml));
                source.Load(colListRoot, items => items);
                source.ExecuteQuery();
                foreach (SP.List oListRoot in colListRoot)
                {
                    try
                    {
                        //var docTemplate = docTemplateCol.OfType<SP.ListTemplate>().FirstOrDefault(t => t.ListTemplateTypeKind == oList.BaseTemplate);
                        //CamlQuery camlQuery = new CamlQuery();
                        //var listItemCollection = oList.GetItems(camlQuery);
                        //source.Load(listItemCollection);
                        //string shemaXml = oList.SchemaXml;
                        //XmlDocument xmlDocument = new XmlDocument();
                        //xmlDocument.LoadXml(shemaXml);
                        //XmlNode listElement = xmlDocument.GetElementsByTagName("List")[0];
                        //string authorid = listElement.Attributes["Author"].Value;
                        //User listAuthor = source.Web.GetUserById(Convert.ToInt32(authorid));
                        //source.Load(listAuthor);

                        source.ExecuteQuery();
                        csv.AppendLine($"{webRoot.ServerRelativeUrl},{oListRoot.BaseType},{oListRoot.Title},{oListRoot.Created.ToString()},{oListRoot.LastItemModifiedDate.ToString()}");
                        // csv.AppendLine($"{web.ServerRelativeUrl},{oList.BaseType},{oList.Title},{oList.Created.ToString()},{listAuthor.Title},{oList.LastItemModifiedDate.ToString()}");
                        Console.WriteLine("SiteUrl:{0}, Type:{1}, Title: {2}, Created: {3}, Updated: {4}", webRoot.ServerRelativeUrl, oListRoot.BaseType, oListRoot.Title, oListRoot.Created.ToString(), oListRoot.LastItemModifiedDate.ToString());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Exception: {0}", ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
            }
            //csv.AppendLine($"ServerRelativeUrl,BaseType,ListTitle,ListCreated,LastItemModifiedDate");
            foreach (Web web in colWeb)
            {
                try
                {
                    //var docTemplateCol = web.ListTemplates;
                    //source.Load(docTemplateCol);
                    //source.ExecuteQuery();
                    ListCollection colList = web.Lists;
                    source.Load(colList, items => items.Include(item => item.LastItemModifiedDate, item=>item.BaseType, item=>item.Created, item=>item.Title, item=>item.SchemaXml));
                    source.ExecuteQuery();
                    foreach (SP.List oList in colList)
                    {
                        try
                        {
                            //var docTemplate = docTemplateCol.OfType<SP.ListTemplate>().FirstOrDefault(t => t.ListTemplateTypeKind == oList.BaseTemplate);
                            //CamlQuery camlQuery = new CamlQuery();
                            //var listItemCollection = oList.GetItems(camlQuery);
                            //source.Load(listItemCollection);
                            //string shemaXml = oList.SchemaXml;
                            //XmlDocument xmlDocument = new XmlDocument();
                            //xmlDocument.LoadXml(shemaXml);
                            //XmlNode listElement = xmlDocument.GetElementsByTagName("List")[0];
                            //string authorid = listElement.Attributes["Author"].Value;
                            //User listAuthor = source.Web.GetUserById(Convert.ToInt32(authorid));
                            //source.Load(listAuthor);

                           source.ExecuteQuery();
                            csv.AppendLine($"{web.ServerRelativeUrl},{oList.BaseType},{oList.Title},{oList.Created.ToString()},{oList.LastItemModifiedDate.ToString()}");
                           // csv.AppendLine($"{web.ServerRelativeUrl},{oList.BaseType},{oList.Title},{oList.Created.ToString()},{listAuthor.Title},{oList.LastItemModifiedDate.ToString()}");
                            Console.WriteLine("SiteUrl:{0}, Type:{1}, Title: {2}, Created: {3}, Updated: {4}",web.ServerRelativeUrl,oList.BaseType, oList.Title, oList.Created.ToString(),  oList.LastItemModifiedDate.ToString());
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Exception: {0}", ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception: {0}", ex.Message);
                }
            }
            Console.WriteLine("Z-Countries retrieved");
            result = csv.ToString();
            return result;
        }
    }
}
