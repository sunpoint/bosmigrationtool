﻿using Microsoft.SharePoint.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;

namespace BOSMigrationToolNetFramework.Business
{
    class Tests
    {
        private readonly ClientContext _ctx;
        public Tests(ClientContext ctx)
        {
            _ctx = ctx;
        }

        public List<string> GetAllLists()
        {
            Console.WriteLine("GetAllLists 1");
            ListCollection targetlListCollection = _ctx.Web.Lists;
            _ctx.Load(targetlListCollection);
            _ctx.ExecuteQuery();
            var result = new List<string>();
            Console.WriteLine("GetAllLists 2");
            foreach (List list in targetlListCollection)
            {
                result.Add(list.Title);
            }
            Console.WriteLine("GetAllLists 2");
            return result;
        }

        public void TestDocumentLibrary()
        {
            List sourceList = _ctx.Web.Lists.GetByTitle("Key Contacts");

            CamlQuery sourceQuery = CamlQuery.CreateAllItemsQuery();
            ListItemCollection sourceItems = sourceList.GetItems(sourceQuery);

            _ctx.Load(sourceItems);
            _ctx.ExecuteQuery();

            var srclibraryname = sourceItems[0].FieldValues["FileDirRef"].ToString();
            string[] srcurlSplit = srclibraryname.Split('/');
            srclibraryname = srcurlSplit[srcurlSplit.Length - 1];
            foreach (ListItem doc in sourceItems)
            {
                var fileName = doc["FileRef"].ToString();
                string[] fileNames = fileName.Split(new string[] { srclibraryname }, StringSplitOptions.None);
                fileName = fileNames[fileNames.Count() - 1];

                File file = doc.File;
                _ctx.Load(file);
                _ctx.ExecuteQuery();

                FileInformation fileInfo = File.OpenBinaryDirect(_ctx, file.ServerRelativeUrl);
                //File.SaveBinaryDirect(_ctx, file.ServerRelativeUrl + "/" + srclibraryname + fileName + "(copy)", fileInfo.Stream, true);
                var nameWithoutExtension = "/sites/Sunpoint/Key Contacts/" + System.IO.Path.GetFileNameWithoutExtension(file.ServerRelativeUrl);
                var extension = System.IO.Path.GetExtension(file.ServerRelativeUrl);
                File.SaveBinaryDirect(_ctx, nameWithoutExtension + "(copy)" + extension, fileInfo.Stream, true);
                break;
            }
        }


        public void RestTest()
        {
            using(var client = new WebClient())
            {
                client.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f");
                SecureString secureString = new SecureString();
                SHPOnlineConstant.PassWord.ToList().ForEach(secureString.AppendChar);
                client.Credentials = new SharePointOnlineCredentials(SHPOnlineConstant.UserName, secureString);
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json;odata=verbose");
                client.Headers.Add(HttpRequestHeader.Accept, "application/json;odata=verbose");
                var endpointUri = new Uri($"{SHPOnlineConstant.URL}/_api/web/lists/getbytitle('KeyContacts')/fields?$filter=Hidden eq false and ReadOnlyField eq false");
                var result = client.DownloadString(endpointUri);
                var result_lists_jtoken = JToken.Parse(result);
                var result_lists_jtoken_array = (JArray)result_lists_jtoken["d"]["results"];
                foreach (var item in result_lists_jtoken_array)
                {
                    Console.WriteLine(item["InternalName"].ToString());
                }
            }
        }

        public void CSOMTest()
        {
            List targetList = _ctx.Web.Lists.GetByTitle("KeyContacts");
            FieldCollection oFieldCollection = targetList.Fields;

            // Here we have loaded Field collection including title of each field in the collection
            _ctx.Load(oFieldCollection);
            _ctx.ExecuteQuery();

            foreach (Field oField in oFieldCollection.Where(f => !f.ReadOnlyField && !f.Hidden))
            {
                Console.WriteLine(oField.InternalName);
            }
        }

        public void CSOMCopyTest()
        {
            List targetList = _ctx.Web.Lists.GetByTitle("FAQ");
            FieldCollection oFieldCollection = targetList.Fields;

            _ctx.Load(oFieldCollection);
            _ctx.ExecuteQuery();

            CamlQuery sourceQuery = CamlQuery.CreateAllItemsQuery();
            ListItemCollection sourceItems = targetList.GetItems(sourceQuery);

            _ctx.Load(sourceItems);
            _ctx.ExecuteQuery();
            int i = 0;

            foreach (ListItem item in sourceItems)
            {
                ListItemCreationInformation creationInfo = new ListItemCreationInformation();
                ListItem destinationItem = targetList.AddItem(creationInfo);
                foreach (Field oField in oFieldCollection.Where(f => !f.ReadOnlyField && !f.Hidden))
                {
                    try
                    {
                        if (oField.InternalName == "Title")
                        {
                            destinationItem[oField.InternalName] = item[oField.InternalName].ToString() + "test";
                        }
                        else
                        {
                            destinationItem[oField.InternalName] = item[oField.InternalName];
                        }
                        destinationItem.Update();
                        _ctx.ExecuteQuery();
                    }
                    catch (Exception ex)
                    {
                        continue;
                    }                 
                }
                i++;
                Console.WriteLine($"{i} of {sourceItems.Count} items created");
            }
        }
    }
}
