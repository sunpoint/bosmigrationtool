﻿using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSMigrationToolNetFramework.Business
{
    class ZLawFirmMasterMapper : BaseMapper
    {
        private readonly Dictionary<int, string> zStateMaster;
        public ZLawFirmMasterMapper(ClientContext sourceCTX, ClientContext destinationCTX)
            : base(sourceCTX, destinationCTX)
        {
            zStateMaster = destinationCTX.GetLookupFromList("Z-State List Master", "State_x0020_Abbreviation");
            //SourceListName = "Z-Law Firm Master"; //"EMPLOYMENT%20CONTRACTS"; 
            //DestinationListName = "Z-Law Firm Master";
        }
        protected override void Map(ListItem source, ListItem destination)
        {
            MapLookups(source, destination);
        }

        private void MapLookups(ListItem source, ListItem destination)
        {
            destination.SetValueFromDictionary(source, zStateMaster, "State");
        }
    }
}
