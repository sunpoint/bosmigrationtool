﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BOSMigrationToolNetFramework.Helpers
{
    static class Extensions
    {
        public static Dictionary<int, string> GetLookupFromList(this ClientContext ctx, string listName, string searchField)
        {
            Console.WriteLine($"trying retrieve values from {listName} by field {searchField}");
            List oList = ctx.Web.Lists.GetByTitle(listName);
            CamlQuery query = CamlQuery.CreateAllItemsQuery();
            ListItemCollection items = oList.GetItems(query);
            ctx.Load(items);
            ctx.ExecuteQuery();
            var result = new Dictionary<int, string>();
            foreach (ListItem item in items)
            {
                result.Add(item.Id, item.ParseStringField(searchField));
            }
            Console.WriteLine($"{listName} retrieved Count{result.Count}");
            return result;
        }

        public static string ParseStringField(this ListItem item, string fieldName)
        {
            return item[fieldName] != null ? item[fieldName].ToString() : string.Empty;
        }

        public static DateTime? ParseDateTimeField(this ListItem item, string fieldName)
        {
            if(item[fieldName] != null)
            {
                return Convert.ToDateTime(item[fieldName].ToString());
            }
            return null;
        }

        public static FieldLookupValue ParseLookupField(this ListItem item, string fieldName)
        {
            return item[fieldName] != null ? item[fieldName] as FieldLookupValue : null;
        }

        public static FieldLookupValue[] ParseLookupArrayField(this ListItem item, string fieldName)
        {
            return item[fieldName] != null ? item[fieldName] as FieldLookupValue[] : null;
        }

        public static void SetValueFromDictionary(this ListItem destination, ListItem source, Dictionary<int, string> dictionary, string destinationField, string sourceField = "")
        {
            if (string.IsNullOrEmpty(sourceField))
            {
                sourceField = destinationField;
            }
            #region For non Multiply Values
            FieldLookupValue lookup = source.ParseLookupField(sourceField);


            if (lookup != null && dictionary.ContainsValue(lookup.LookupValue))
            {
                var destinationCompany = dictionary.First(e => e.Value.Equals(lookup.LookupValue));
                //Console.WriteLine($"Value from dictionary {destinationCompany}");
                FieldLookupValue lv = new FieldLookupValue();
                lv.LookupId = destinationCompany.Key;
                destination[destinationField] = lv;
            }
            #endregion
            #region For Multiply Values
            //FieldLookupValue[] lookup = source.ParseLookupArrayField(sourceField);
            //if (lookup != null)
            //{
            //    var newIds = new FieldLookupValue[lookup.Length];
            //    for (int i = 0; i < lookup.Length; i++)
            //    {
            //        var value = dictionary.First(e => e.Value.Equals(lookup[i].LookupValue));
            //        FieldLookupValue lv = new FieldLookupValue();
            //        lv.LookupId = value.Key;
            //        newIds[i] = lv;
            //    }
            //    destination[destinationField] = newIds;
            //}
            #endregion
        }

        public static bool ParseBoolField(this ListItem item, string fieldName)
        {
            return item[fieldName] != null ? Convert.ToBoolean(item[fieldName].ToString()) : false;
        }
    }
}
