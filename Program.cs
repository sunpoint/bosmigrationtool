﻿using BOSMigrationToolNetFramework.Business;
using BOSMigrationToolNetFramework.Business.ListToLibrary;
using BOSMigrationToolNetFramework.Business.Mappers;
using BOSMigrationToolNetFramework.Helpers;
using Microsoft.SharePoint.Client;
using System;
using System.Linq;
using System.Security;
using System.Text;

namespace BOSMigrationToolNetFramework
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    //string[] sites =  { "/", "/sites/acelloswms", "/sites/AEI", "/sites/ahg", "/sites/BIPDEV", "/sites/clinical", "/sites/clinical2010"
        //    //,"/sites/dynamicserp","/sites/NGS","/sites/PSG","/sites/scport","/sites/T2010"
        //    //};

        //    string[] sites =  { "/sites/clinical2010"
        //    };

        //    //var filepath = "C:\\Users\\dmitrip\\Downloads\\bosmigrationtool\\report.csv";
        //    var filepath = "C:\\Users\\dmitrip\\Downloads\\bosmigrationtool\\report_clinical2010.csv";
        //    var csv = new StringBuilder();
        //    csv.AppendLine($"ServerRelativeUrl,BaseType,ListTitle,ListCreated,LastItemModifiedDate");
        //    System.IO.File.WriteAllText(filepath, csv.ToString());
        //    foreach (var site_url in sites)
        //    {
        //        ClientContext sourceContext = new ClientContext(SHP2010Constant.FarmURL+site_url);
        //        try
        //        {
        //            System.IO.File.AppendAllText(filepath, ReportClass.GetSites(sourceContext));
        //            Console.WriteLine("Done");
        //        }
        //        catch (Exception ex)
        //        {
        //            var csvNEwLine = new StringBuilder();
        //            csvNEwLine.AppendLine($"{site_url} Error {ex.Message}");
        //            System.IO.File.AppendAllText(filepath, csvNEwLine.ToString());
        //            Console.WriteLine($"{site_url} Error {ex.Message}");
        //        }
        //        finally
        //        {
        //            if (sourceContext != null)
        //                sourceContext.Dispose();

        //            //Console.WriteLine("Press any key to exit");
        //            //Console.ReadKey();
        //        }
        //    }

        //}
        static void Main(string[] args)
        {
            ClientContext sourceContext = new ClientContext(SHP2010Constant.URL);
            ClientContext destinationContext = new ClientContext(SHPOnlineConstant.URL);
            try
            {
                SecureString secureString = new SecureString();
                SHPOnlineConstant.PassWord.ToList().ForEach(secureString.AppendChar);
                var credentials = new SharePointOnlineCredentials(SHPOnlineConstant.UserName, secureString);
                destinationContext.Credentials = credentials;
                //var tests = new Tests(destinationContext);
                //tests.GetAllLists();
                //foreach (var item in tests.GetAllLists())
                //{
                //    Console.WriteLine(item);
                //}
                //tests.RestTest();
                //tests.CSOMTest();
                //tests.CSOMCopyTest();
                var employmentMapper = new EmploymentMasterMapper(sourceContext, destinationContext);
                employmentMapper.Map("Z-Physician List Master", "Z-Physician List Master");
                //var employmentMapper = new EmploymentMasterMapper(sourceContext, destinationContext);
                //employmentMapper.Map("Annual Reports", "Company Information");
                #region ListMappers
                //var mapper = new ZLawFirmMasterMapper(sourceContext, destinationContext);
                //mapper.Map("Z-RSO Master", "Z-RSO Master");
                //var mapper = new ZLawFirmMasterMapper(sourceContext, destinationContext);
                //mapper.Map("Consents", "Consents");
                #endregion

                #region DocumentLibraryMappers
                //var mapper = new RealPropertyMapper(sourceContext, destinationContext); //this
                //var mapper = new EmploymentMapper(sourceContext, destinationContext);
                //var mapper = new CorporateDocsMapper(sourceContext, destinationContext); //this
                //var mapper = new GeneralDocumentsMapper(sourceContext, destinationContext); //this
                //var mapper = new PhysicanAgreementsMaper(sourceContext, destinationContext);
                //var mapper = new ModelReleases(sourceContext, destinationContext);
                //mapper.Map();
                #endregion

                #region ListToDocumentLibraryMappers
                //var mapper = new RINGIListMapper(sourceContext, destinationContext);
                //var mapper = new BusinessLicensesMapper(sourceContext, destinationContext);
                //var mapper = new PhysicianLicensing(sourceContext, destinationContext);
                //var mapper = new AnnualReportsMapper(sourceContext, destinationContext);
                //var mapper = new LegalInvoices(sourceContext, destinationContext);
                //mapper.CreateFields();
                //var excp = mapper.Map();
                //foreach (var item in excp)
                //{
                //    Console.WriteLine(item);
                //}
                #endregion

                //var comparer = new Comparers(sourceContext, destinationContext);
                //comparer.Compare("Z-RSO Master", "Associated_x0020_RSO_x002f_CO", "Associated_x0020_RSO_x002f_CO");
                //comparer.Compare("Company Information", "Title", "Title");
                Console.WriteLine("Done");
        }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sourceContext != null)
                    sourceContext.Dispose();
                if (destinationContext != null)
                    destinationContext.Dispose();

                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
        }
    }
}
